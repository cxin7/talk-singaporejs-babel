# Babel Plugins: Writing Code That Writes Code

Overview of Babel and introduction to ASTs for fame and fortune.

## Demo

Create Babel plugins to traverse and transform code. Build a linter, minifier, transpiler, etc.

## Slides

[![](http://i.giphy.com/1XuzR5X4cWdl6.gif)](https://gitpitch.com/sebdeckers/talk-singaporejs-babel?grs=gitlab)

## Video

[![Video thumbnail](https://img.youtube.com/vi/HPJSoIRYeG4/0.jpg)](https://engineers.sg/video/babel-plugins-writing-code-that-writes-code-singaporejs--1233)

## Proposal

<https://github.com/SingaporeJS/organizers/issues/37>
