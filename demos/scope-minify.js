import {transform} from 'babel-core'
const source =
  `function incrementVariable (somethingReallyLong) {
    return somethingReallyLong + 1 }`
const {code} = transform(source, {
  plugins: [{
    visitor: {
      Identifier (path) {
        if (path.node.name.length > 2) {
          path.scope.rename(
            path.node.name,
            path.scope.generateUidIdentifier('_').name
          )
        }
      }
    }
  }]
})
console.log(code)
