import {transform} from 'babel-core'
const {code} = transform('alert("hello world")', {
  plugins: [({types: t}) => ({
    visitor: {
      CallExpression (path) {
        if (t.isIdentifier(path.node.callee, {name: 'alert'})) {
          path.get('callee').replaceWith(
            t.memberExpression(
              t.identifier('console'),
              t.identifier('log'))
          )
        }
      }
    }
  })]
})
console.log(code)
