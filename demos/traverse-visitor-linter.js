import {transform} from 'babel-core'
transform('var Foo = 123; var bar = 456;', {
  plugins: [{
    visitor: {
      VariableDeclarator (path) {
        const name = path.node.id.name
        const firstLetter = name.at(0)
        if (firstLetter === firstLetter.toLocaleUpperCase()) {
          console.warn(`"${name}" begins with an uppercase letter.`)
        }
      }
    }
  }]
})
